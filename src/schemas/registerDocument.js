var mongoose = require('mongoose');   

exports.registerDocumentModel = function(){
    var registerDocumentSchema = mongoose.Schema({
        _id: mongoose.Schema.Types.ObjectId,
        invoiceDocumentType: String,
        invoiceDocumentNumber: Number,
        company: String,
        co: String,
        resolutionAuthorizationNumber: String,
        resolutionInitialControlNumber: Number,
        resolutionFinalControlNumber: Number,
        expeditionDate: String,
        expirationDate: String,
        authorizationType: String
    });

    let registerDocument;

    try{
        registerDocument = mongoose.model('registerDocument');
    } catch(error){
        registerDocument = mongoose.model('registerDocument', registerDocumentSchema);
    }

    return registerDocument;
}