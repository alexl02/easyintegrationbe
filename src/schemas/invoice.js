var mongoose = require('mongoose');
var registerSchema = require('../schemas/register');
var registerDocumentSchema = require('../schemas/registerDocument');

exports.invoiceModel = function(){
    var invoiceSchema = mongoose.Schema({
        _id: mongoose.Schema.Types.ObjectId,
        registerDate: String,
        co: String,
        registerNumber: String,
        registerDocumentNumber: Number,
        invoiceDocumentType: String,
        invoiceDocumentNumber: Number,
        paymentType: String,
        paymentMethodCode: String,
        ingressEgressIndicator: String,
        paymentValue: Number,
        state: Number,
        register: String,
        items: []
    });

    invoiceSchema.pre('save', function(next) {
        var doc = this;
        var registerM = registerSchema.registerModel();
        var registerDocumentM = registerDocumentSchema.registerDocumentModel();
        
        registerM.findByIdAndUpdate( { _id: doc.register }, {$inc: { registerDocumentNumber: 1} }, function(error, register){
            if(error){
                return next(error);
            } else {
                registerDocumentM.findByIdAndUpdate( { _id: register.registerDocument }, {$inc: { invoiceDocumentNumber: 1} }, function(error, registerDocument){
                    if(error){
                        return next(error);
                    } else {
                        doc.registerDocumentNumber = register.registerDocumentNumber;
                        doc.invoiceDocumentNumber = registerDocument.invoiceDocumentNumber;
                        next();
                    }
                });
            }
            
        });
    });

    let invoice;

    try{
        invoice = mongoose.model('invoice');
    } catch(error){
        invoice = mongoose.model('invoice', invoiceSchema);
    }

    return invoice;
}