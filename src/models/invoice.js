var mongoose = require('mongoose');   
var invoiceSchema = require('../schemas/invoice');
var registerSchema = require('../schemas/register');
var registerDocumentSchema = require('../schemas/registerDocument');
var companySchema = require('../schemas/company');
var crypto = require('crypto');
var fomatDate = require('../../formatDate');

exports.findAll = function(req, res){
    var invoiceM = invoiceSchema.invoiceModel();
    
    console.log("Reading invoices...");
    invoiceM.find().then(
        invoices => {
            res.status(200).jsonp(invoices);
        },
        err => {
            console.log("An error was ocurred reading invoices.");
            res.status(500).send(err.message);
        }
    );
}

exports.findById = function(req, res){
    var invoiceM = invoiceSchema.invoiceModel();
    if(!req.params.id){
        res.status(400).send("No valid value");
    }
    console.log("Reading invoice...");
    invoiceM.findById( req.params.id ).then(
        invoice => {
            res.status(200).jsonp(invoice);
        },
        err => {
            console.log("An error was ocurred reading invoice.");
            res.status(500).send(err.message);
        }
    );
    
}

exports.findByRegister = function(req, res){
    var invoiceM = invoiceSchema.invoiceModel();
    if (!req.params.register){
        res.status(400).send("No valid data");
    } else {
        console.log("Reading invoice...");
        invoiceM.find( { register: { _id: req.param.register } } ).then(
            invoice => {
                res.status(200).jsonp(invoice);
            },
            err => {
                console.log("An error was ocurred reading invoice.");
                res.status(500).send(err.message);
            }
        );    
    }

}

exports.findByDocument = function(req, res){
    var invoiceM = invoiceSchema.invoiceModel();
    if ((!req.body.register) || (!req.body.invoiceDocumentType) || (!req.body.invoiceDocumentNumber)){
        res.status(400).send("No valid data");
    } else {
        console.log("Reading invoice...");
        invoiceM.find( { register: { _id: req.param.register }, invoiceDocumentType: req.body.invoiceDocumentType, invoiceDocumentNumber: req.body.invoiceDocumentNumber } ).then(
            invoice => {
                res.status(200).jsonp(invoice);
            },
            err => {
                console.log("An error was ocurred reading invoice.");
                res.status(500).send(err.message);
            }
        );    
    }

}

exports.save = async (req, res, sockets) => {
    if ((!req.body.register) || (!req.body.paymentMethodCode) || (!req.body.paymentValue) || (!req.body.items) || (!req.body.items > 0)){
        res.status(400).send("No valid data");
        return 0;
    }

    var invoiceM = invoiceSchema.invoiceModel();
    var companyM = companySchema.companyModel();
    var registerM = registerSchema.registerModel();
    var registerDocumentM = registerDocumentSchema.registerDocumentModel();

    

    let register = await registerM.findById(req.body.register);
    if(!register){
        res.status(404).send("An error was ocurred saving invoice, register not found.");
        return 0;
    }
    
    let company = await companyM.findById(register.company);
    if(!company){
        res.status(404).send("An error was ocurred saving invoice, company not found.");
        return 0;
    }
    
    let registerDocument = await registerDocumentM.findById(register.registerDocument);
    if(!registerDocument){
        res.status(404).send("An error was ocurred saving invoice, register document not found.");
        return 0;
    }
    

    /*invoiceTemp = await invoiceM.findOne( { nit: req.body.nit } );
    if((!req.body._id) && (invoiceTemp)){
        console.log(invoiceTemp);
        res.status(403).send("The nit is already registered");
        console.log("The nit is already registered");
        return 0;
    }*/

    invoiceTemp = {
        _id: new mongoose.Types.ObjectId(),
        registerDate: fomatDate.formatDateMin2(new Date()),
        co: register.co,
        registerNumber: register.registerNumber,
        //registerDocumentNumber: registerDocumentNumber,
        invoiceDocumentType: registerDocument.invoiceDocumentType,
        //invoiceDocumentNumber: invoiceDocumentNumber,
        paymentType: "1",
        paymentMethodCode: req.body.paymentMethodCode,
        ingressEgressIndicator: "I",
        paymentValue: req.body.paymentValue,
        state: 1,
        register: register._id,
        items: req.body.items
    }

    if(invoiceTemp){
        invoice = new invoiceM(invoiceTemp);

        console.log("saving invoice");
        invoiceNew = await invoice.save();

        if(invoiceNew){
            let socket = sockets.find((socketClientF) => {
            //return socketClientF.companyId === company._id;
                return socketClientF.companyId === '5f47f7f0c8ae1e183e5dcb25';
            }).socket;
            socket.emit('invoice', invoiceNew);
            res.status(200).jsonp(invoiceNew);
            return 1;
        } else {
            console.log("An error was ocurred saving invoice.");
            res.status(500).send("An error was ocurred saving invoice.")
        }
    } else {
        console.log("An error was ocurred modifiying invoice, invoice not found.");
        res.status(500).send("An error was ocurred modifiying invoice, invoice not found.");
    }

    return 0;
}

