var mongoose = require('mongoose');   

exports.userModel = function(){
    var userSchema = mongoose.Schema({
        _id: mongoose.Schema.Types.ObjectId,
        name: String,
        userName: String,
        password: String,
        createAt: Date,
        lastLogin: Date,
    });

    let user;

    try{
        user = mongoose.model('user');
    } catch(error){
        user = mongoose.model('user', userSchema);
    }

    return user;
}