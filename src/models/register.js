var mongoose = require('mongoose');   
var registerSchema = require('../schemas/register');
var registerDocumentSchema = require('../schemas/registerDocument');
var companySchema = require('../schemas/company');
var crypto = require('crypto');

exports.findAll = function(req, res){
    var registerM = registerSchema.registerModel();
    
    console.log("Reading registers...");
    registerM.find().then(
        registers => {
            res.status(200).jsonp(registers);
        },
        err => {
            console.log("An error was ocurred reading registers.");
            res.status(500).send(err.message);
        }
    );
}

exports.findById = function(req, res){
    var registerM = registerSchema.registerModel();
    if(!req.params.id){
        res.status(400).send("No valid value");
    }
    console.log("Reading register...");
    registerM.findById( req.params._id ).then(
        register => {
            res.status(200).jsonp(register);
        },
        err => {
            console.log("An error was ocurred reading register.");
            res.status(500).send(err.message);
        }
    );
    
}

exports.findByRegisterNumber = function(req, res){
    var registerM = registerSchema.registerModel();
    if(!req.params.id){
        res.status(400).send("No valid value");
    }
    console.log("Reading register...");
    registerM.find( { registerNumber: req.params.registerNumber } ).then(
        register => {
            res.status(200).jsonp(register);
        },
        err => {
            console.log("An error was ocurred reading register.");
            res.status(500).send(err.message);
        }
    );
    
}

exports.findByCompanyAndCoAndRegisterNumber = function(req, res){
    var registerM = registerSchema.registerModel();
    if ((!req.body.company) || (!req.body.co) || (!req.body.registerNumber)){
        res.status(400).send("No valid data");
    } else {
        console.log("Reading register...");
        registerM.findOne( { company: {_id: req.body.company}, co: req.body.company, registerNumber: req.params.registerNumber } ).then(
            register => {
                res.status(200).jsonp(register);
            },
            err => {
                console.log("An error was ocurred reading register.");
                res.status(500).send(err.message);
            }
        );
    }
    
    
    
}

exports.save = async (req, res) => {
    if ((!req.body.registerNumber) || (!req.body.name) || (!req.body.co) || ((!req.body.registerDocumentNumber) && (req.body.registerDocumentNumber === undefined)) || (!req.body.company) || (!req.body.registerDocument)){
        res.status(400).send("No valid data");
        return 0;
    }

    var registerM = registerSchema.registerModel();
    var companyM = companySchema.companyModel();
    var registerDocumentM = registerDocumentSchema.registerDocumentModel();

    const company = await companyM.findById(req.body.company);
    const registerDocument = await registerDocumentM.findById(req.body.registerDocument);

    if(!company){
        console.log("An error was ocurred saving register, company not found.");
        res.status(404).send("An error was ocurred saving register, company not found.");
        return 0;
    }

    if(!registerDocument){
        console.log("An error was ocurred saving register, register document not found.");
        res.status(404).send("An error was ocurred saving register, register document not found.");
        return 0;
    }

    /*registerTemp = await registerM.findOne( { nit: req.body.nit } );
    if((!req.body._id) && (registerTemp)){
        console.log(registerTemp);
        res.status(403).send("The nit is already registered");
        console.log("The nit is already registered");
        return 0;
    }*/



    registerTemp = {
        _id: new mongoose.Types.ObjectId(),
        registerNumber: req.body.registerNumber,
        name: req.body.name,
        co: req.body.co,
        registerDocumentNumber: req.body.registerDocumentNumber,
        company: req.body.company,
        registerDocument: req.body.registerDocument,
    }

    if(req.body._id){
        registerTemp = await registerM.findById(req.body._id);
        registerTemp.registerNumber = req.body.registerNumber;
        registerTemp.name = req.body.name;
        registerTemp.co = req.body.co;
        registerTemp.company = req.body.company;
        registerTemp.registerDocument = req.body.registerDocument;
    } 

    if(registerTemp){
        register = new registerM(registerTemp);

        console.log("saving register");
        registerNew = await register.save();

        if(registerNew){
            res.status(200).jsonp(registerNew);
            return 1;
        } else {
            console.log("An error was ocurred saving register.");
            res.status(500).send("An error was ocurred saving register.")
        }
    } else {
        console.log("An error was ocurred modifiying register, register not found.");
        res.status(500).send("An error was ocurred modifiying register, register not found.");
    }

    return 0;
}

