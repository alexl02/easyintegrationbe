var mongoose = require('mongoose');   
var companySchema = require('../schemas/company');
var crypto = require('crypto');

exports.findAll = function(req, res){
    var companyM = companySchema.companyModel();
    
    console.log("Reading Companies...");
    companyM.find().then(
        companies => {
            res.status(200).jsonp(companies);
        },
        err => {
            console.log("An error was ocurred reading companies.");
            res.status(500).send(err.message);
        }
    );
}

exports.findById = function(req, res){
    var companyM = companySchema.companyModel();
    if(!req.params.id){
        res.status(400).send("No valid value");
    }
    console.log("Reading company...");
    companyM.findById( req.params.id ).then(
        company => {
            res.status(200).jsonp(company);
        },
        err => {
            console.log("An error was ocurred reading company.");
            res.status(500).send(err.message);
        }
    );
    
}

exports.findByNit = function(req, res){
    var companyM = companySchema.companyModel();
    if(!req.params.nit){
        res.status(400).send("No valid value");
    }
    console.log("Reading company...");
    companyM.findOne( { nit: req.params.nit } ).then(
        company => {
            res.status(200).jsonp(company);
        },
        err => {
            console.log("An error was ocurred reading company.");
            res.status(500).send(err.message);
        }
    );    
}

exports.save = async (req, res) => {
    if ((!req.body.name) || (!req.body.nit) || (!req.body.password)){
        res.status(400).send("No valid data");
        return 0;
    }

    var companyM = companySchema.companyModel();

    companyTemp = await companyM.findOne( { nit: req.body.nit } );
    if((!req.body._id) && (companyTemp)){
        console.log(companyTemp);
        res.status(403).send("The nit is already registered");
        console.log("The nit is already registered");
        return 0;
    }

    companyTemp = {
        _id: new mongoose.Types.ObjectId(),
        nit: req.body.nit,
        name: req.body.name,
        password: crypto.createHash('md5').update(req.body.password).digest("hex"),
        createAt: req.body.createAt ? req.body.createAt : new Date()
    }

    if(req.body._id){
        companyTemp = await companyM.findById(req.body._id);
        companyTemp.nit = req.body.nit;
        companyTemp.name = req.body.name;
        companyTemp.password = req.body.password ? crypto.createHash('md5').update(req.body.password).digest("hex") : companyTemp.password;
    } 

    if(companyTemp){
        company = new companyM(companyTemp);

        console.log("saving company");
        companyNew = await company.save();

        if(companyNew){
            res.status(200).jsonp(companyNew);
            return 1;
        } else {
            console.log("An error was ocurred saving company.");
            res.status(500).send("An error was ocurred saving company.")
        }
    } else {
        console.log("An error was ocurred modifiying company, company not found.");
        res.status(500).send("An error was ocurred modifiying company, company not found.");
    }

    return 0;
}

