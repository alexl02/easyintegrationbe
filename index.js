const express = require("express"); // instalar
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const config = require('./config');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const mongoose = require('mongoose');
const invoiceMethods = require('./src/http-methods/invoice');
const httpCompany = require('./src/models/company');
const httpRegister = require('./src/models/register');
const httpRegisterDocument = require('./src/models/registerDocument');
const httpInvoice = require('./src/models/invoice');
const httpUser = require('./src/models/user');

app.set('key', config.key);

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

app.use(bodyParser.urlencoded({ extended: false })); //para la lectura de json
app.use(bodyParser.json()); //para la lectura de json

let socketsClient = [];
io.on('connection', function(socket){
    let socketClient = {}
    socketClient.socket = socket;
    console.log('un cliente se ha conectado');
    socket.emit('identify')
    
    socket.on('disconnect', function(){
        console.log('Usuario desconectado');
        let socketIndex = socketsClient.findIndex((socketClientF => {
            return socketClientF.socket = socket;
        }));
        socketsClient.splice(socketIndex, 1);
    });

    socket.on('identify', (data) => {
        socketClient.companyId = data.companyId;
        socketsClient.push(socketClient);
        console.log("Cliente identificado.");
    });

    socket.on('invoiceReception', (data) => {
        if(data.status === "OK"){
            console.log("Factura " + data.invoiceNumber + " recepcionada correctamente.");
        } else {
            console.log("Error al recepcionar la factura " + data.invoiceNumber + ".");
        }
    });
});

const protectedRoutes = express.Router();

protectedRoutes.use((req, res, next) => {
    const token = req.headers['access-token'];
 
    if (token) {
      jwt.verify(token, app.get('key'), (err, decoded) => {      
        if (err) {
          return res.json({ mensaje: 'invalid token.' });    
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      res.send({ 
          mensaje: 'No token sended.'
      });
    }
 });

app.get('/company', /*protectedRoutes,*/ httpCompany.findAll);
app.get('/company/:id', /*protectedRoutes,*/ httpCompany.findById);
app.get('/company/nit/:nit', /*protectedRoutes,*/ httpCompany.findByNit);
app.post('/company', /*protectedRoutes,*/ (req, res) => {
    httpCompany.save(req, res).then(
        res => {
            if(res === 1){
                console.log("Success, the company was created/modified");
            } else {
                console.log("An error was ocurred saving company.");    
            }
        },
        err => {
            console.log("An error was ocurred saving company.");
            console.log(err.message);
        }
    );
});

app.get('/register', /*protectedRoutes,*/ httpRegister.findAll);
app.get('/register/:id', /*protectedRoutes,*/ httpRegister.findById);
app.get('/register/registerNumber/:registerNumeber', /*protectedRoutes,*/ httpRegister.findByRegisterNumber);
app.get('/register/companyRegister', /*protectedRoutes,*/ httpRegister.findByCompanyAndCoAndRegisterNumber);
app.post('/register', /*protectedRoutes,*/ (req, res) => {
    httpRegister.save(req, res).then(
        res => {
            if(res === 1){
                console.log("Success, the register was created/modified");
            } else {
                console.log("An error was ocurred saving register.");    
            }
        },
        err => {
            console.log("An error was ocurred saving register.");
            console.log(err.message);
        }
    );
});

app.get('/registerDocument', /*protectedRoutes,*/ httpRegisterDocument.findAll);
app.get('/registerDocument/:id', /*protectedRoutes,*/ httpRegisterDocument.findById);
app.get('/registerDocument/documentType/:documentType', /*protectedRoutes,*/ httpRegisterDocument.findByInvoiceDocumentType);
app.get('/registerDocument/co/:co', /*protectedRoutes,*/ httpRegisterDocument.findByCo);
app.get('/registerDocument/resolution/:resolution', /*protectedRoutes,*/ httpRegisterDocument.findByResolutionAuthorizationNumber);
app.get('/registerDocument/CompanyRegisterDocument', /*protectedRoutes,*/ httpRegisterDocument.findByCompanyAndDocumentTypeAndCo);
app.post('/registerDocument', /*protectedRoutes,*/ (req, res) => {
    httpRegisterDocument.save(req, res).then(
        res => {
            if(res === 1){
                console.log("Success, the register document was created/modified");
            } else {
                console.log("An error was ocurred saving register document.");    
            }
        },
        err => {
            console.log("An error was ocurred saving register document.");
            console.log(err.message);
        }
    );
});

app.get('/invoice', /*protectedRoutes,*/ httpInvoice.findAll);
app.get('/invoice/:id', /*protectedRoutes,*/ httpInvoice.findById);
app.get('/invoice/register/:register', /*protectedRoutes,*/ httpInvoice.findByRegister);
app.get('/invoice/document', /*protectedRoutes,*/ httpInvoice.findByDocument);
app.post('/invoice', /*protectedRoutes,*/ (req, res) => {
    httpInvoice.save(req, res, socketsClient).then(
        res => {
            if(res === 1){
                console.log("Success, the invoice was created/modified");
            } else {
                console.log("An error was ocurred saving invoice.");    
            }
        },
        err => {
            console.log("An error was ocurred saving invoice.");
            console.log(err.message);
        }
    );
});

app.get('/user', protectedRoutes, httpUser.findAll);
app.get('/user/:id', protectedRoutes, httpUser.findById);
app.get('/user/userName/:userName', protectedRoutes, httpUser.findByUserName);
app.post('/user/login', (req, res) => {
    httpUser.login(req, res, app.get('key'));
});
app.post('/user', protectedRoutes, (req, res) => {
    httpUser.save(req, res).then(
        res => {
            if(res === 1){
                console.log("Success, the user was created/modified");
            } else {
                console.log("An error was ocurred saving user.");    
            }
        },
        err => {
            console.log("An error was ocurred saving user.");
            console.log(err.message);
        }
    );
});

/*app.post('/invoice', (req, res) => {
    invoiceMethods.postInvoice(req, res, socketsClient);
});*/

mongoose.set('useUnifiedTopology', true);
mongoose.connect('mongodb://root:sistemas@192.168.20.193:27017/EasyIntegration?authSource=admin', { useNewUrlParser: true, useFindAndModify: false })
.then(
    () => {
        console.log("Conected to de database correctly.")      
        server.listen(8080, function(){
            console.log('Servidor corriendo en http://localhost:8080');
        });
    },
    
)
.catch(
    err => {
        console.log("Ocurrio un error al conectar a la base de datos")
        console.log(err);
    }
);

