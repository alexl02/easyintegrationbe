var mongoose = require('mongoose');   
var registerDocumentSchema = require('../schemas/registerDocument');
//var invoiceDocumentSchema = require('../schemas/registerDocument');
var crypto = require('crypto');
var companySchema = require('../schemas/company');

exports.findAll = function(req, res){
    var registerDocumentM = registerDocumentSchema.registerDocumentModel();
    
    console.log("Reading registerDocuments...");
    registerDocumentM.find().then(
        registerDocuments => {
            res.status(200).jsonp(registerDocuments);
        },
        err => {
            console.log("An error was ocurred reading registerDocuments.");
            res.status(500).send(err.message);
        }
    );
}

exports.findById = function(req, res){
    var registerDocumentM = registerDocumentSchema.registerDocumentModel();
    if(!req.params.id){
        res.status(400).send("No valid value");
    }
    console.log("Reading registerDocument...");
    registerDocumentM.findById( req.params.id ).then(
        registerDocument => {
            res.status(200).jsonp(registerDocument);
        },
        err => {
            console.log("An error was ocurred reading registerDocument.");
            res.status(500).send(err.message);
        }
    );
    
}

exports.findByInvoiceDocumentType = function(req, res){
    var registerDocumentM = registerDocumentSchema.registerDocumentModel();
    if(!req.params.invoiceDocumentType){
        res.status(400).send("No valid value");
    }
    console.log("Reading registerDocument...");
    registerDocumentM.find( { invoiceDocumentType: req.params.invoiceDocumentType } ).then(
        registerDocument => {
            res.status(200).jsonp(registerDocument);
        },
        err => {
            console.log("An error was ocurred reading registerDocument.");
            res.status(500).send(err.message);
        }
    );    
}

exports.findByCo = function(req, res){
    var registerDocumentM = registerDocumentSchema.registerDocumentModel();
    if(!req.params.co){
        res.status(400).send("No valid value");
    }
    console.log("Reading registerDocument...");
    registerDocumentM.find( { co: req.params.co } ).then(
        registerDocument => {
            res.status(200).jsonp(registerDocument);
        },
        err => {
            console.log("An error was ocurred reading registerDocument.");
            res.status(500).send(err.message);
        }
    );    
}

exports.findByResolutionAuthorizationNumber = function(req, res){
    var registerDocumentM = registerDocumentSchema.registerDocumentModel();
    if(!req.params.resolutionAuthorizationNumber){
        res.status(400).send("No valid value");
    }
    console.log("Reading registerDocument...");
    registerDocumentM.findOne( { resolutionAuthorizationNumber: req.params.resolutionAuthorizationNumber } ).then(
        registerDocument => {
            res.status(200).jsonp(registerDocument);
        },
        err => {
            console.log("An error was ocurred reading registerDocument.");
            res.status(500).send(err.message);
        }
    );    
}

exports.findByCompanyAndDocumentTypeAndCo = function(req, res){
    var registerDocumentM = registerDocumentSchema.registerDocumentModel();
    if ((!req.body.company) || (!req.body.co) || (!req.body.documentType)){
        res.status(400).send("No valid data");
    } else {
        console.log("Reading registerDocument...");
        registerDocumentM.findOne( { company: { _id: req.body.company._id}, invoiceDocumentType: req.body.invoiceDocumentType, co: req.body.co } ).then(
            registerDocument => {
                res.status(200).jsonp(registerDocument);
            },
            err => {
                console.log("An error was ocurred reading registerDocument.");
                res.status(500).send(err.message);
            }
        );    
    }

}

exports.save = async (req, res) => {
    if (
        (!req.body.invoiceDocumentType) || 
        ((!req.body.invoiceDocumentNumber) && (req.body.invoiceDocumentNumber === undefined)) || 
        (!req.body.company) || 
        (!req.body.co) || 
        (!req.body.resolutionAuthorizationNumber) || 
        (!req.body.resolutionInitialControlNumber) || 
        (!req.body.resolutionFinalControlNumber) || 
        (!req.body.expeditionDate) || 
        (!req.body.expirationDate) || 
        (!req.body.authorizationType))
    {
        res.status(400).send("No valid data");
        return 0;
    }

    var registerDocumentM = registerDocumentSchema.registerDocumentModel();
    var companyM = companySchema.companyModel();

    const company = await companyM.findById(req.body.company);

    if(!company){
        console.log("An error was ocurred saving register, company not found.");
        res.status(404).send("An error was ocurred saving register, company not found.");
        return 0;
    }

    /*registerDocumentTemp = await registerDocumentM.findOne( { nit: req.body.nit } );
    if((!req.body._id) && (registerDocumentTemp)){
        console.log(registerDocumentTemp);
        res.status(403).send("The nit is already registered");
        console.log("The nit is already registered");
        return 0;
    }*/

    registerDocumentTemp = {
        _id: new mongoose.Types.ObjectId(),
        invoiceDocumentType: req.body.invoiceDocumentType,
        invoiceDocumentNumber: req.body.invoiceDocumentNumber,
        company: req.body.company,
        co: req.body.co,
        resolutionAuthorizationNumber: req.body.resolutionAuthorizationNumber,
        resolutionInitialControlNumber: req.body.resolutionInitialControlNumber,
        resolutionFinalControlNumber: req.body.resolutionFinalControlNumber,
        expeditionDate: req.body.expeditionDate,
        expirationDate: req.body.expirationDate,
        authorizationType: req.body.authorizationType
    }
        

    if(req.body._id){
        registerDocumentTemp = await registerDocumentM.findById(req.body._id);
        registerDocumentTemp.invoiceDocumentNumber = req.body.invoiceDocumentNumber;
        registerDocumentTemp.resolutionAuthorizationNumber = req.body.resolutionAuthorizationNumber;
        registerDocumentTemp.resolutionInitialControlNumber = req.body.resolutionInitialControlNumber;
        registerDocumentTemp.resolutionFinalControlNumber = req.body.resolutionFinalControlNumber;
        registerDocumentTemp.expeditionDate = req.body.expeditionDate;
        registerDocumentTemp.expirationDate = req.body.expirationDate;
        registerDocumentTemp.authorizationType = req.body.authorizationType;
    } 

    if(registerDocumentTemp){
        registerDocument = new registerDocumentM(registerDocumentTemp);

        console.log("saving registerDocument");
        registerDocumentNew = await registerDocument.save();

        if(registerDocumentNew){
            res.status(200).jsonp(registerDocumentNew);
            return 1;
        } else {
            console.log("An error was ocurred saving registerDocument.");
            res.status(500).send("An error was ocurred saving registerDocument.")
        }
    } else {
        console.log("An error was ocurred modifiying registerDocument, registerDocument not found.");
        res.status(500).send("An error was ocurred modifiying registerDocument, registerDocument not found.");
    }

    return 0;
}

/*exports.save = async (req, res) => {
    var registerDocumentM = registerDocumentSchema.registerDocumentModel();
    
    registerDocumentTemp = {
        _id: new mongoose.Types.ObjectId(),
        invoiceDocumentType: "2F",
        invoiceDocumentNumber: 0,
        company: "5f47f7f0c8ae1e183e5dcb25",
        co: "002",
        resolutionAuthorizationNumber: "18763000506166",
        resolutionInitialControlNumber: 1,
        resolutionFinalControlNumber: 999999,
        expeditionDate: "20190917",
        expirationDate: "20210916",
        authorizationType: "A"
    }
        

    registerDocument = new registerDocumentM(registerDocumentTemp);

    console.log("saving registerDocument");
    registerDocumentNew = await registerDocument.save();

    if(registerDocumentNew){
        res.status(200).jsonp(registerDocumentNew);
        return 1;
    } else {
        console.log("An error was ocurred saving registerDocument.");
        res.status(500).send("An error was ocurred saving registerDocument.")
    }

    return 0;
}*/

