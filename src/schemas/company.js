var mongoose = require('mongoose');   

exports.companyModel = function(){
    var companySchema = mongoose.Schema({
        _id: mongoose.Schema.Types.ObjectId,
        nit: String,
        name: String,
        password: String,
        createAt: Date
    });

    let company;

    try{
        company = mongoose.model('company');
    } catch(error){
        company = mongoose.model('company', companySchema);
    }

    return company;
}