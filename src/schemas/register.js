var mongoose = require('mongoose');   

exports.registerModel = function(){
    var registerSchema = mongoose.Schema({
        _id: mongoose.Schema.Types.ObjectId,
        registerNumber: String,
        name: String,
        co: String,
        registerDocumentNumber: Number,
        company: String,
        registerDocument: String
    });

    let register;

    try{
        register = mongoose.model('register');
    } catch(error){
        register = mongoose.model('register', registerSchema);
    }

    return register;
}