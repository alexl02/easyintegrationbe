var mongoose = require('mongoose');   
var userSchema = require('../schemas/user');
var crypto = require('crypto');
const jwt = require('jsonwebtoken');

exports.findAll = function(req, res){
    var userM = userSchema.userModel();
    
    console.log("Reading users...");
    userM.find().then(
        users => {
            res.status(200).jsonp(users);
        },
        err => {
            console.log("An error was ocurred reading users.");
            res.status(500).send(err.message);
        }
    );
}

exports.findById = function(req, res){
    var userM = userSchema.userModel();
    if(!req.params.id){
        res.status(400).send("No valid value");
    }
    console.log("Reading user...");
    userM.find( { _id: req.params.id} ).then(
        user => {
            res.status(200).jsonp(config);
        },
        err => {
            console.log("An error was ocurred reading configuration.");
            res.status(500).send(err.message);
        }
    );
    
}

exports.findByUserName = function(req, res){
    var userM = userSchema.userModel();
    if(!req.params.userName){
        res.status(400).send("No valid value");
    }
    console.log("Reading user...");
    userM.find( { userName: req.params.userName } ).then(
        user => {
            res.status(200).jsonp(user);
        },
        err => {
            console.log("An error was ocurred reading user.");
            res.status(500).send(err.message);
        }
    );    
}

exports.login = function(req, res, key){
    var userM = userSchema.userModel();
    if(!req.body.userName){
        res.status(400).send("No valid value");
    }
    console.log("Reading user...");
    userM.findOne( { userName: req.body.userName, password: crypto.createHash('md5').update(req.body.password).digest("hex")} ).then(
        user => {
            if(user){
                const payload = {
                    check:  true
                };
                const token = jwt.sign(payload, key, {
                    expiresIn: 1440
                });
                res.status(200).jsonp(token);
            } else {
                console.log("Login failed.");
                res.status(401).send("Login failed.");
            }
        },
        err => {
            console.log("An error was ocurred reading user.");
            res.status(500).send(err.message);
        }
    );    
}

exports.save = async (req, res) => {
    if ((!req.body.name) || (!req.body.userName) || (!req.body.password)){
        res.status(400).send("No valid data");
        return 0;
    }

    var userM = userSchema.userModel();

    userTemp = await userM.findOne( { userName: req.body.userName } );
    if((!req.body._id) && (userTemp)){
        console.log(userTemp);
        res.status(403).send("The user name is already registered");
        console.log("The user name is already registered");
        return 0;
    }

    userTemp = {
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        userName: req.body.userName,
        password: crypto.createHash('md5').update(req.body.password).digest("hex"),
        createAt: req.body.createAt ? req.body.createAt : new Date()
    }

    if(req.body._id){
        userTemp = await userM.findById(req.body._id);
        userTemp.name = req.body.name,
        userTemp.userName = req.body.userName,
        userTemp.password = req.body.password ? crypto.createHash('md5').update(req.body.password).digest("hex") : userTemp.password;
    }

    if(userTemp){
        user = new userM(userTemp);

        console.log("saving user");
        userNew = await user.save();

        if(userNew){
            userNew.password = undefined;
            res.status(200).jsonp(userNew);
            return 1;
        } else {
            console.log("An error was ocurred saving user.");
            res.status(500).send("An error was ocurred saving user.")
        }
    } else {
        console.log("An error was ocurred modifiying user, user not found.");
        res.status(500).send("An error was ocurred modifiying user, user not found.");
    }

    return 0;
}

